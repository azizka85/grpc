﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace gRPC.External.API.Service.Controllers
{
    [Route("[controller]")]
    public class GreetController : Controller
    {
        private ILogger<GreetController> _logger;
        
        public GreetController(ILogger<GreetController> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        
        [HttpGet("{name}")]
        public IActionResult Greet(string name, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Hello, {name}!");
            
            return Ok($"Hello, {name}!");
        }
    }
}