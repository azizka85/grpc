﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using gRPC.External.API.Service.Protos.Services;
using Microsoft.Extensions.Logging;

namespace gRPC.External.API.Service.GrpcServices
{
    public sealed class TimingService : Timing.TimingBase
    {
        private ILogger<GreetService> _logger;

        public TimingService(ILogger<GreetService> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public override async Task Call(CallRequest request, IServerStreamWriter<CallResponse> responseStream, ServerCallContext context)
        {
            try
            {
                for (int i = 0; i < 100; i++)
                {
                    _logger.LogInformation($"Write {i} times");
                
                    await responseStream.WriteAsync(
                        new CallResponse()
                        {
                            Timestamp = Timestamp.FromDateTime(DateTime.UtcNow),
                            Message = $"{i}: Hello, {request.Name}!"
                        });
                
                    Thread.Sleep(1500);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, context.Status.Detail);
            }
        }
    }
}