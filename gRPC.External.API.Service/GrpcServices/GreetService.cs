﻿using System;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using gRPC.External.API.Service.Protos.Services;
using Microsoft.Extensions.Logging;

namespace gRPC.External.API.Service.GrpcServices
{
    public sealed class GreetService : Greet.GreetBase
    {
        private ILogger<GreetService> _logger;

        public GreetService(ILogger<GreetService> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public override async Task<GetResponse> Get(GetRequest request, ServerCallContext context)
        {
            _logger.LogInformation($"{context.Host}, {context.Method}, {context.Peer}, {context.Status}, {context.RequestHeaders}");
            
            return new GetResponse()
            {
                Timestamp = Timestamp.FromDateTime(DateTime.UtcNow),
                Message = $"Hello, {request.Name}!",
            };
        }
    }
}