using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace gRPC.External.API.Service
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                WebHost
                    .CreateDefaultBuilder(args)
                    .SuppressStatusMessages(true)
                    .ConfigureAppConfiguration(
                        (builderContext, config) => 
                            config
                                .AddJsonFile("appsettings.secrets.json", true, true)
                                .AddJsonFile("/run/secrets/appsettings.json", true, true)
                                .AddEnvironmentVariables())
                    .ConfigureLogging(builder =>
                    {
                        builder.ClearProviders();
                        builder.AddConsole();
                    })
                    .ConfigureKestrel((context, serverOptions) =>
                    {
                        var endPointIndex = 0;
                        serverOptions.ConfigureEndpointDefaults(options =>
                        {
                            if (endPointIndex % 2 == 0)
                            {
                                options.Protocols = HttpProtocols.Http2;
                            }
                            else
                            {
                                options.Protocols = HttpProtocols.Http1AndHttp2;
                            }
                            
                            Console.WriteLine($"{endPointIndex}, {options.Protocols}, {options.EndPoint}");
                            
                            endPointIndex++;
                        });
                    })
                    .UseStartup<Startup>()
                    .Build()
                    .Run();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }
    }
}