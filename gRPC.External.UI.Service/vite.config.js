﻿import {defineConfig, loadEnv} from 'vite'

export default defineConfig(async ({ command, mode }) => {
  const env = loadEnv(mode, process.cwd(), '')
  
  const plugins = []
  
  if(mode === 'development') {
    const react = await (await import('@vitejs/plugin-react')).default
    
    plugins.push(react())
  }
  
  return {
    mode,
    base: env.REACT_APP_PageRoot ?? '/',
    server: {
      port: env.PORT ?? 3000,
      host: true
    },
    preview: {
      port: env.PORT ?? 8276,
      host: true
    },
    css: {
      modules: {
        localsConvention: "camelCase"
      }
    },
    plugins,
    envPrefix: [
      'VITE_',
      'REACT_APP_'
    ]
  }
})

