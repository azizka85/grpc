﻿import { GrpcWebFetchTransport } from '@protobuf-ts/grpcweb-transport'

const grpcTransport = new GrpcWebFetchTransport({
  baseUrl: import.meta.env.REACT_APP_GrpcApi,
  format: import.meta.env.REACT_APP_GrpcFormat
})

const inject = {
  grpcTransport
}

export default inject