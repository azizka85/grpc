﻿import React, {useEffect, useRef, useState} from 'react'

import inject from '../inject'

import { TimingClient } from '../protos/timing.client'
import { CallResponse } from '../protos/timing'
import { Timestamp } from '../protos/google/protobuf/timestamp'

export default function () {
  const inputRef = useRef<HTMLInputElement>(null)
  const [messages, setMessages] = useState<CallResponse[]>([])
  const [abortController, setAbortController] = useState<AbortController | null>(null);
  const client = new TimingClient(inject.grpcTransport)
  
  useEffect(() => () => cancel(), [abortController])
  
  function call() {
    messages.length = 0
    
    const abortController = new AbortController()
    const abort = abortController.signal

    setAbortController(abortController)
    
    const streamingCall = client.call({
      name: inputRef.current?.value || 'RAMS Signature'
    }, {
      abort
    })
  
    streamingCall.responses.onMessage(message => {
      messages.push(message)
    
      setMessages([
        ...messages
      ])
    })
  
    streamingCall.responses.onError(error => {
      console.error(error)
    })
  }
  
  function cancel() {
    abortController?.abort('client canceled')
    console.log(abortController?.signal, 'aborted:', abortController?.signal.aborted, abortController?.signal.reason)
  }
  
  return (
    <div>
      <h3>Stream gRPC type</h3>
      <div>
        <input
          ref={inputRef}
          type="text"
          placeholder="Enter a name"
        />
        &nbsp;
        <button
          onClick={() => call()}
        >
          Call
        </button>
        &nbsp;
        <button
          onClick={() => cancel()}
        >
          Cancel
        </button>
      </div>
      <br />
      <div>
        <ol>
          {messages.map((message, index) => (
            <li key={index}>
              {Timestamp.toDate(message.timestamp ?? Timestamp.now()).toLocaleString()}
              -
              {message.message}
            </li>
          ))}
        </ol>
      </div>
    </div>
  )
}