﻿import React from 'react'
import GrpcUnary from './grpc-unary'
import GrpcStream from './grpc-stream'

export default function () {
  return (
    <div>
      <h3>List of gRPC methods</h3>
      <GrpcUnary />
      <hr />
      <GrpcStream />
      <hr />
      <GrpcStream />
    </div>
  )
}