﻿import React, {useRef, useState} from 'react'

import inject from '../inject'

import { GreetClient } from '../protos/greet.client'

export default function () {
  const inputRef = useRef<HTMLInputElement>(null)
  const [message, setMessage] = useState('Empty message')
  const client = new GreetClient(inject.grpcTransport)
  
  async function submit() {
    const res = await client.get({
      name: inputRef.current?.value || 'Boombati Residence!'
    })
    
    setMessage(res.response.message)
  }
  
  return (
    <div>
      <h3>Unary gRPC type</h3>
      <div>
        <input 
          ref={inputRef}
          type="text" 
          placeholder="Enter a name"
        />
        &nbsp;
        <button
          onClick={() => submit()}
        >
          Submit
        </button>
      </div>
      <br />
      <div>
        {message}
      </div>
    </div>
  )
}