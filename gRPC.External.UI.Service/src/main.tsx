﻿import React from 'react'
import ReactDOM from 'react-dom/client'

import GrpcMethodsList from './components/grpc-methods-list'

ReactDOM.createRoot(
  document.getElementById('root') as Element
).render(
  <GrpcMethodsList />
)