﻿# gRPC
gRPC testing on the server and on the browser

- `gRPC.External.API.Service` folder where server is located and it was written on ASPNetCore
- `gRPC.External.UI.Service` folder where browser app is located and it was written on React

## Installation

### Server
- only need Rider

### Client
- go to `gRPC.External.UI.Service` directory
- run: `npm i`

## Deploy

### Server
- just run the project: `gRPC.External.API.Service`
- server listened: 
- - `http://localhost:8274` for http/2 
- - `https://localhost:8275` for http/1 and http/2 

### Client
Need to run following commands:
- development: `npm run dev`, by default app listened port: 3000
- production: `npm run build` after `npm run preview`, by default app listened port: 8276
- listening port you can change in .env.local file



